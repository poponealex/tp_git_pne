#include <stdlib.h>
#include <stdio.h>
#include "geometry.h"
#include "list.h"
#include "tree.h"
#include "algo.h"

int main() {
  // Segment d'origine (6,1) et d'extr ́emit ́e (6,6)
  struct Segment s1 = { { {6,1}, {1,1} }, { {6,1}, {6,1} } }; 
  // Segment d'origine (1,1) et d'extr ́emit ́e (6,6)
  struct Segment s2 = { { {1,1}, {1,1} }, { {6,1}, {6,1} } };

  display segment(s1) ;
  display segment(s2) ;

  allPairs((char*) "data/input", (char*) "data/output");
  BentleyOttmmann((char*) "data/input", (char*) "data/output2");
  return EXIT_SUCCESS;
}
